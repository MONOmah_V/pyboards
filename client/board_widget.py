from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.Qt import *


class BoardWidget(QWidget):
    line_drawn = pyqtSignal(QPoint, QPoint, name='lineDrawn')
    curve_drawn = pyqtSignal(list, name='curveDrawn')

    def __init__(self, parent=None):
        super(BoardWidget, self).__init__(parent)

        self.setAttribute(Qt.WA_StaticContents)
        self._drawing = False
        self._image = QImage()
        self._pen_width = 1
        self._pen_color = QColor(Qt.blue)
        self._mode = 0 # 0 - line, 1 - curve
        self._press_point = QPoint()
        self._polyline = []

    @property
    def pen_color(self):
        return self._pen_color

    @pen_color.setter
    def pen_color(self, color):
        self._pen_color = color

    @property
    def pen_width(self):
        return self._pen_width

    @pen_width.setter
    def pen_width(self, width):
        self._pen_width = width

    @pyqtSlot(str, name='setDrawingMode')
    def setDrawingMode(self, mode):
        self._mode = mode

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self._press_point = event.pos()
            self._drawing = True

    def mouseMoveEvent(self, event):
        if self._drawing and self._mode == 1:
            self._polyline.append(event.pos())

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            if self._mode == 0:
                self.__draw_line_to(event.pos())
            elif self._mode == 1:
                self.__draw_curve(self._polyline)
                self._polyline = []
            self._drawing = False

    def paintEvent(self, event):
        painter = QPainter(self)
        dirty_rect = event.rect()
        painter.drawImage(dirty_rect, self._image, dirty_rect)

    def resizeEvent(self, event):
        if self.width() > self._image.width() or self.height() > self._image.height():
            newWidth = max(self.width() + 128, self._image.width())
            newHeight = max(self.height() + 128, self._image.height())
            self.__resize_image(self._image, QSize(newWidth, newHeight))
            self.update()
        super(BoardWidget, self).resizeEvent(event)

    def __resize_image(self, image, newSize):
        if image.size() == newSize:
            return
        newImage = QImage(newSize, QImage.Format_RGB32)
        newImage.fill(qRgb(255, 255, 255))
        painter = QPainter(newImage)
        painter.drawImage(QPoint(0, 0), image)
        self._image = newImage

    def clear_image(self):
        self._image.fill(qRgb(255, 255, 255))
        self.update()

    def __draw_curve(self, points):
        self.draw_curve(points, self.pen_color, self.pen_width)
        self.curve_drawn.emit(points)  # возможно тут будет херня, ибо список очистится выше

    def draw_curve(self, points, color=Qt.blue, width=1):
        painter = QPainter(self._image)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setPen(QPen(color, width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        painter.drawPolyline(*points)
        self.update()

    def __draw_line_to(self, endPoint):
        self.draw_line_to(self._press_point, endPoint, self.pen_color, self.pen_width)
        self.line_drawn.emit(self._press_point, endPoint)
        self._press_point = QPoint(endPoint)

    def draw_line_to(self, p1, p2, color=Qt.blue, width=1):
        painter = QPainter(self._image)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setPen(QPen(color, width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
        painter.drawLine(p1, p2)
        rad = width / 2 + 2
        self.update(QRect(p1, p2).normalized().adjusted(-rad, -rad, +rad, +rad))
