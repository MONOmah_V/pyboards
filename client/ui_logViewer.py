# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'client\logViewer.ui'
#
# Created: Thu Nov 13 14:00:50 2014
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog_logViewer(object):
    def setupUi(self, Dialog_logViewer):
        Dialog_logViewer.setObjectName("Dialog_logViewer")
        Dialog_logViewer.resize(696, 464)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog_logViewer)
        self.verticalLayout.setObjectName("verticalLayout")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(Dialog_logViewer)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.verticalLayout.addWidget(self.plainTextEdit)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog_logViewer)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog_logViewer)
        self.buttonBox.accepted.connect(Dialog_logViewer.accept)
        self.buttonBox.rejected.connect(Dialog_logViewer.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog_logViewer)

    def retranslateUi(self, Dialog_logViewer):
        _translate = QtCore.QCoreApplication.translate
        Dialog_logViewer.setWindowTitle(_translate("Dialog_logViewer", "Log View"))

