from client.ui_logViewer import Ui_Dialog_logViewer
from PyQt5.QtWidgets import QDialog


class LogViewer(QDialog):
    def __init__(self, parent=None):
        super(LogViewer, self).__init__(parent)
        self.ui = Ui_Dialog_logViewer()
        self.ui.setupUi(self)
