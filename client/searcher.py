from PyQt5.QtCore import *
_translate = QCoreApplication.translate
import logging
import socket


module_logger = logging.getLogger('pyBoards')


class Searcher(QObject):
    """
    Class implements server discovery service and
    intended to be in separate thread as explained in [8] of project doc.

    Available signals:
    search_complete(set) - emitting when search finished with set of found
        servers, presented like a tuple of (address, port, name)
        or with an empty set.
    search_failed() - emitting when something goes wrong with socket.
    finished() - emitting upon main loop stopped. It should be connect()-ed to
        instances thread slot quit().
    """
    search_complete = pyqtSignal(set)
    search_failed = pyqtSignal()
    finished = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self._logger = logging.getLogger(module_logger.name+'.client.searcher')
        self._logger.debug(_translate('Searcher', 'initialized'))

    @pyqtSlot()
    def search(self):
        """
        Search for available servers in network via catching all udp packets
        on port ('0.0.0.0', 13131) for five (or a bit more) seconds.
        If packet matches the 'pyBoards@<PORT>' pattern, then sender of
        this packet is added to the set of servers.
        Emits search_complete(servers) when done.
        """
        self._logger.info(_translate('Searcher', 'started'))
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        servers = set()
        time = QTime()
        try:
            sock.bind(('', 13131))
            sock.settimeout(5)
            time.start()
            while time.elapsed() < 5000:
                try:
                    data, addr = sock.recvfrom(128)
                    name, sep, port = data.decode().partition('@')
                    if name.startswith('pyBoards'):
                        port = int(port)
                        addr = addr[0]
                        if (addr, port, name) not in servers:
                            servers.add((addr, port, name))
                except socket.timeout:
                    pass
        except Exception as e:
            self._logger.exception('fixme:' + _translate('Searcher',
                                                         'search failed'))
            self.search_failed.emit()
        else:
            self._logger.info(_translate('Searcher', 'search complete'))
            self.search_complete.emit(servers)
        finally:
            sock.close()
            self.finished.emit()


if __name__ == '__main__':
    print('This is Searcher')
