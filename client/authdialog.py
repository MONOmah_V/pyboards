from client.ui_authDialog import Ui_Dialog
from PyQt5.QtWidgets import QDialog

class AuthDialog(QDialog):
    def __init__(self, parent = None):
        super(AuthDialog, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

    @staticmethod
    def getCredentials(parent = None):
        dialog = AuthDialog(parent)
        result = dialog.exec_()
        return (dialog.ui.lineEdit_login.text(), dialog.ui.lineEdit_password.text(),
                result == QDialog.Accepted)