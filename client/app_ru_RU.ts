<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>ChatClient</name>
    <message>
        <location filename="chat_client.py" line="33"/>
        <source>initialized</source>
        <translation>инициализирован</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="51"/>
        <source>Cannot change server while in connected state</source>
        <translation>Невозможно изменить сервер при активном подключении</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="56"/>
        <source>connecting</source>
        <translation>подключение</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="64"/>
        <source>connection refused by server</source>
        <translation>сервер отверг запрос на подключение</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="74"/>
        <source>connection failed</source>
        <translation>соединение разорвано</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="79"/>
        <source>connected to {}:{}</source>
        <translation>подключен к {}:{}</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="94"/>
        <source>disconnected</source>
        <translation>отключен</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="113"/>
        <source>started</source>
        <translation>запущен</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="128"/>
        <source>server disconnected</source>
        <translation>сервер отключился</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="138"/>
        <source>Connection reset by server</source>
        <translation>Удаленный хост принудительно разорвал подключение</translation>
    </message>
    <message>
        <location filename="chat_client.py" line="144"/>
        <source>connection dropped: </source>
        <translation>соединение сброшено: </translation>
    </message>
    <message>
        <location filename="chat_client.py" line="152"/>
        <source>stopped</source>
        <translation>остановлен</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="authdialog.ui" line="14"/>
        <source>Authentication</source>
        <translation>Аутентификация</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="22"/>
        <source>admin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="29"/>
        <source>Login</source>
        <translation>Логин</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="36"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="authdialog.ui" line="46"/>
        <source>qwerty</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Dialog_logViewer</name>
    <message>
        <location filename="logViewer.ui" line="14"/>
        <source>Log View</source>
        <translation>Просмотр журнала
</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>pyBoards</source>
        <translation>pyBoards</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="118"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Servers</source>
        <translation>Серверы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="316"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <source>Disconnect</source>
        <translation>Отключиться</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Refresh</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="204"/>
        <source>Toolbox</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="224"/>
        <source>Pen width</source>
        <translation>Ширина пера</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <source>Select color</source>
        <translation>Выбрать цвет</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <source>Line</source>
        <translation>Линия</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="254"/>
        <source>Curve</source>
        <translation>Кривая</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="303"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="37"/>
        <source>initialized</source>
        <translation>инициализирован</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="170"/>
        <source>Disconnected</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="77"/>
        <source>exiting</source>
        <translation>завершение работы</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="135"/>
        <source>Search failed</source>
        <translation>Поиск не выполнен</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="144"/>
        <source>Select a server!</source>
        <translation>Выберите сервер!</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="153"/>
        <source>Connected to {}:{}</source>
        <translation>Подключен к {}:{}</translation>
    </message>
    <message>
        <location filename="main.py" line="148"/>
        <source>Connection failed: </source>
        <translation type="obsolete">Соединение разорвано: </translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="242"/>
        <source>malformed message: &quot;{}&quot;</source>
        <translation>некорректное сообщение: &quot;{}&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Connection</source>
        <translation>Подключение</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="94"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="90"/>
        <source>pyBoards -- Graphics chat (client)
(C) 2014 Borisova Natalia, Vassyutovich Ilya
BMSTU, Software for Computers and Automation Systems, IU7-71</source>
        <translation>pyBoards -- Графический чат (клиент)
(С) 2014 Борисова Наталья, Васютович Илья
МГТУ им. Баумана, Информатика и системы управления, ИУ7-71</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="326"/>
        <source>Logs</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="212"/>
        <source>Connection failed</source>
        <translation>Соединение разорвано</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="86"/>
        <source>started</source>
        <translation>запущен</translation>
    </message>
</context>
<context>
    <name>Searcher</name>
    <message>
        <location filename="searcher.py" line="20"/>
        <source>initialized</source>
        <translation>инициализирован</translation>
    </message>
    <message>
        <location filename="searcher.py" line="50"/>
        <source>search failed</source>
        <translation>поиск не выполнен</translation>
    </message>
    <message>
        <location filename="searcher.py" line="53"/>
        <source>search complete</source>
        <translation>поиск завершен</translation>
    </message>
    <message>
        <location filename="searcher.py" line="28"/>
        <source>started</source>
        <translation>запущен</translation>
    </message>
</context>
</TS>
