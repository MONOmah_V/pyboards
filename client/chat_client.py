from PyQt5.QtCore import *
_translate = QCoreApplication.translate
import logging
import socket
import tools.msocket as msocket


module_logger = logging.getLogger('pyBoards')


class ChatClient(QObject):
    """
    Class implements network services for client application and
    intended to be in separate thread as explained in [8] of project doc.
    User should specify server via server.setter before calling process().

    Available signals:
    messageReceived(str) - emitted upon message received.
        Contains received message
    connected() - emitted upon successful connection.
    disconnected() - emitted upon disconnection.
    connection_failed(str) - emitted upon connection failure, contains reason.
    stopped() - emitted upon main loop stopped. It should be connect()-ed to
        instances thread slot quit().
    """
    messageReceived = pyqtSignal(str)
    connected = pyqtSignal()
    disconnected = pyqtSignal()
    connection_failed = pyqtSignal(str)
    stopped = pyqtSignal()

    def __init__(self, parent=None):
        """
        Creates instance of ChatClient. After creation,
        user need to specify server's address via server.setter,
        e. g. chatter.server = ('127.0.0.1', 12345).
        """
        super().__init__(parent)
        self._logger = logging.getLogger(module_logger.name+'.client.chatclient')
        self._server = ()
        self._address = ()
        self._is_connected = False
        self._mutex = QMutex()
        self._shutting_down = False
        self._logger.debug(_translate('ChatClient', 'initialized'))

    @property
    def is_connected(self):
        return self._is_connected

    @property
    def address(self):
        return self._address

    @property
    def server(self):
        return self._server

    @server.setter
    def server(self, address):
        if self._is_connected:
            raise RuntimeError(_translate('ChatClient',
                                          'Cannot change server while in connected state'))
        self._server = address

    def __connect(self):
        """
        Try to connect to specified server. Emit connected or
        connection_failed upon done.
        """
        self._logger.debug(_translate('ChatClient', 'connecting'))
        try:
            self._sock = msocket.msocket()
            self._sock.bind(('', 0))
            self._sock.connect(self._server)
            self._sock.setblocking(True)
            self._sock.settimeout(3)
        except ConnectionRefusedError as e:
            message = _translate('ChatClient', 'connection refused by server')
            self._logger.warning(message)
            self.connection_failed.emit(message)
            raise e
        except ConnectionError as e:
            self._logger.exception('fixme:' + _translate('ChatClient',
                                                         'connection failed'))
            self.connection_failed.emit(e.strerror)
            raise e
        except Exception as e:
            self._logger.exception('fixme:' + _translate('ChatClient',
                                                         'connection failed'))
            self.connection_failed.emit(str(e))
            raise e
        else:
            self._logger.info(_translate('ChatClient', 'connected to {}:{}')
                              .format(*self._server))
            self._is_connected = True
            self._address = self._sock.getsockname()
            self.connected.emit()

    def __disconnect(self):
        """
        Close connection to server and emits disconnected.
        """
        try:
            self._sock.shutdown(socket.SHUT_RDWR)
        except:
            pass
        finally:
            self._sock.close()
            if self._is_connected:
                self.disconnected.emit()
                self._logger.info(_translate('ChatClient', 'disconnected'))
                self._is_connected = False
            self._shutting_down = False

    @pyqtSlot()
    def disconnect_from_server(self):
        """
        Tell the instance to stop working and disconnect from server.
        """
        self._mutex.lock()
        self._shutting_down = True
        self._mutex.unlock()

    @pyqtSlot()
    def process(self):
        """
        Main loop. Handle connecting, disconnecting and message receiving.
        """
        self._logger.info(_translate('ChatClient', 'started'))
        try:
            self.__connect()
        except:
            pass
        else:
            while self._is_connected:
                self._mutex.lock()
                if self._shutting_down:
                    self._mutex.unlock()
                    break
                self._mutex.unlock()
                try:
                    data = self._sock.recv_message()
                    if not data:
                        self._logger.warning(_translate('ChatClient',
                                                        'server disconnected'))
                        break
                    message = data.decode(errors='replace').rstrip()
                    self.messageReceived.emit(message)
                except socket.timeout:
                    pass
                except ConnectionResetError as e:
                    message = _translate('ChatClient',
                                         'Connection reset by server')
                    self._logger.warning(message)
                    self.connection_failed.emit(message)
                    break
                except Exception as e:
                    message = ('fixme:' + _translate('ChatClient',
                                                     'connection dropped: ') +
                               str(e))
                    self._logger.exception(message)
                    self.connection_failed.emit(message)
                    break
        finally:
            self.__disconnect()
        self._logger.info(_translate('ChatClient', 'stopped'))
        self.stopped.emit()

    @pyqtSlot(str)
    def send_message(self, message):
        """
        Send message to connected server
        :param message: DECODED message
        """
        if not self._is_connected:
            raise RuntimeError(_translate('ChatClient',
                                          'No connection to server'))
        self._sock.send_message(message.encode())


if __name__ == '__main__':
    print('This is Chatter')
