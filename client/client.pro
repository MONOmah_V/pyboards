SOURCES += \
    mainwindow.py \
    chat_client.py \
    searcher.py

FORMS    += \
    mainwindow.ui \
    authdialog.ui \
    logViewer.ui

TRANSLATIONS += \
    app_ru_RU.ts
