import client.ui_mainWindow
from client.authdialog import AuthDialog
from client.logViewer import LogViewer
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from client.searcher import Searcher
from client.chat_client import ChatClient
from tools.authentication import generate_challenge, generate_response
import tools.logger
import logging


_translate = QCoreApplication.translate
module_logger = tools.logger.get_logger('client')


class MalformedMessageError(Exception):
    pass


class MainWindow(QMainWindow):
    """
    Main window of application.
    """
    def __init__(self, parent=None):
        """
        Constructor. Setups UI, creates threads for Searcher and ChatClient.
        """
        super(MainWindow, self).__init__(parent)
        self.ui = client.ui_mainWindow.Ui_MainWindow()
        self.ui.setupUi(self)
        self._logger = logging.getLogger(module_logger.name+'.client.ui')
        self.__setup_searcher()
        self.__setup_chat_client()

        self._servers = []
        self.__setup_status_bar()
        self._logger.debug(_translate('MainWindow', 'initialized'))

    def __setup_status_bar(self):
        self.connection_status_widget = QLabel(_translate('MainWindow',
                                                          'Disconnected'))
        self.statusBar().addPermanentWidget(self.connection_status_widget)

    def __setup_searcher(self):
        """
        Create and prepare Searcher and thread for it.
        """
        self.searcher = Searcher()
        self.searcher_thread = QThread(self)
        self.searcher.moveToThread(self.searcher_thread)
        self.searcher_thread.started.connect(self.searcher.search)
        self.searcher.search_complete.connect(self.on_searcher_search_complete)
        self.searcher.search_failed.connect(self.on_searcher_search_failed)
        self.searcher.finished.connect(self.searcher_thread.quit)

    def __setup_chat_client(self):
        """
        Create and prepare ChatClient and thread for it.
        """
        self.chatclient = ChatClient()
        self.chatclient_thread = QThread(self)
        self.chatclient.moveToThread(self.chatclient_thread)
        self.chatclient_thread.started.connect(self.chatclient.process)
        self.chatclient.stopped.connect(self.chatclient_thread.quit)

        self.chatclient.messageReceived.connect(
            self.on_chatclient_messageReceived)
        self.chatclient.connected.connect(self.on_chatclient_connected)
        self.chatclient.disconnected.connect(self.on_chatclient_disconnected)
        self.chatclient.connection_failed.connect(
            self.on_chatclient_connection_failed)

    def closeEvent(self, e):
        """
        Form close handler. Stop and wait for Searcher and ChatClient
        to exit properly.
        """
        self._logger.info(_translate('MainWindow', 'exiting'))
        self.setEnabled(False)
        self.searcher_thread.quit()
        self.searcher_thread.wait()
        self.chatclient.disconnect_from_server()
        self.chatclient_thread.quit()
        self.chatclient_thread.wait()

    def showEvent(self, *args, **kwargs):
        """
        Called upon first form show, needed to create a log entry with
        'started' message.
        """
        self._logger.info(_translate('MainWindow', 'started'))

    @pyqtSlot(name='on_actionAbout_triggered')
    def about(self):
        """
        Show 'about' message box.
        """
        message = _translate("MainWindow",
                             "pyBoards -- Graphics chat (client)\n"
        "(C) 2014 Borisova Natalia, Vassyutovich Ilya\n"
        "BMSTU, Software for Computers and Automation Systems, IU7-71")
        QMessageBox.about(self, _translate('MainWindow', 'About'), message)

    @pyqtSlot(name='on_actionShowLogs_triggered')
    def show_logs(self):
        """
        Search for log-file of current session and pastes it to log_viewer.
        """
        log_viewer = LogViewer(self)
        for handler in self._logger.parent.handlers:
            if isinstance(handler, logging.FileHandler):
                for entry in open(handler.baseFilename, 'r', 1):
                    log_viewer.ui.plainTextEdit.appendPlainText(entry.rstrip())
                break
        log_viewer.exec()

    @pyqtSlot(name='on_actionRefresh_triggered')
    @pyqtSlot(name='on_pushButton_refresh_clicked')
    def start_search(self):
        self.ui.pushButton_refresh.setEnabled(False)
        self.ui.actionRefresh.setEnabled(False)
        self._servers = []
        self.ui.listWidget_servers.clear()
        self.searcher_thread.start()

    @pyqtSlot(set)
    def on_searcher_search_complete(self, servers):
        for server in servers:
            if server not in self._servers:
                self._servers.append(server)

        self.ui.listWidget_servers.clear()
        for server in self._servers:
            self.ui.listWidget_servers.addItem('{2}@{0}:{1}'.format(*server))
        self.ui.pushButton_refresh.setEnabled(True)
        self.ui.actionRefresh.setEnabled(True)

    @pyqtSlot()
    def on_searcher_search_failed(self):
        self.statusBar().showMessage(_translate('MainWindow', 'Search failed'),
                                     10000)
        self.ui.pushButton_refresh.setEnabled(True)

    @pyqtSlot(name='on_pushButton_connect_clicked')
    @pyqtSlot(name='on_actionConnect_triggered')
    def connect(self):
        row = self.ui.listWidget_servers.currentRow()
        if row == -1:
            self.statusBar().showMessage(_translate('MainWindow',
                                                    'Select a server!'),
                                         10000)
            return
        self.chatclient.server = self._servers[row][:2]
        self.chatclient_thread.start()

    @pyqtSlot()
    def on_chatclient_connected(self):
        message = (_translate('MainWindow', 'Connected to {}:{}')
                   .format(*self.chatclient.server))
        self.connection_status_widget.setText(message)
        self.on_chatclient_messageReceived('self:'+message)
        self.ui.pushButton_connect.setEnabled(False)
        self.ui.pushButton_disconnect.setEnabled(True)
        self.ui.pushButton_sendMessage.setEnabled(True)

    @pyqtSlot(name='on_pushButton_disconnect_clicked')
    @pyqtSlot(name='on_actionDisconnect_triggered')
    def disconnect(self):
        self.chatclient.disconnect_from_server()

    @pyqtSlot()
    def on_chatclient_disconnected(self):
        self.connection_status_widget.setText(_translate('MainWindow',
                                                         'Disconnected'))
        self.on_chatclient_messageReceived('self:' + _translate('MainWindow',
                                                                'Disconnected'))
        self.ui.pushButton_connect.setEnabled(True)
        self.ui.pushButton_disconnect.setEnabled(False)
        self.ui.pushButton_sendMessage.setEnabled(False)

    @pyqtSlot(str)
    def on_chatclient_connection_failed(self, message):
        title = _translate('MainWindow', 'Connection failed')
        self.statusBar().showMessage(title + ': ' + message, 10000)
        QMessageBox.critical(self, title, message)

    @pyqtSlot(str)
    def on_chatclient_messageReceived(self, message):
        """
        Main message handler. See project doc for thorough description of proto.
        We really need to find a way to split this logic from form.
        """
        sep = ':'
        header, sep, body = message.partition(sep)
        try:
            if not sep and not body:
                raise MalformedMessageError(message)
            elif header.startswith('auth'):
                # Authentication stuff
                if header == 'authsc':
                    # Request login and password from user
                    login, password, result = AuthDialog.getCredentials(self)
                    sc = body
                    if not result:
                        self.on_pushButton_disconnect_clicked()
                        return
                    cc = generate_challenge()
                    cr = generate_response(sc, cc, login, password)
                    self.chatclient.send_message('authcr:{login}#{cc}#{cr}'
                                                 .format(login=login,
                                                         cc=cc,
                                                         cr=cr))
                else:
                    raise MalformedMessageError(message)
            elif header == 'srv':
                self.ui.plainTextEdit_chat.appendHtml('<b>{}</b>'.format(body))
            elif header == 'term':
                self._logger.warning(body)
                QMessageBox.critical(self, _translate('MainWindow',
                                                      'Connection failed'),
                                     body)
            elif header == 'self':
                self.ui.plainTextEdit_chat.appendHtml('<i>{}</i>'.format(body))
            elif header == 'msg':
                self.ui.plainTextEdit_chat.appendPlainText(body)
            elif header.startswith('drw'):
                #drwl:с(0,0,255)#w(1)#p1(283,133)#p2(170,323)
                #drwc:c(0,0,255)#w(1)#p0(46,364)#p0(47,364)#p0(48,364)
                cont = body.split('#')
                r, g, b = map(int, cont[0][2:-1].split(','))
                w = int(cont[1][2:-1])
                if header == 'drwl':
                    p1x, p1y = map(int, cont[2][3:-1].split(','))
                    p2x, p2y = map(int, cont[3][3:-1].split(','))
                    self.ui.boardWidget.draw_line_to(QPoint(p1x, p1y),
                                                     QPoint(p2x, p2y),
                                                     QColor(r, g, b), w)
                elif header == 'drwc':
                    points = []
                    for point in cont[2:]:
                        x, y = map(int, point[3:-1].split(','))
                        points.append(QPoint(x, y))
                    self.ui.boardWidget.draw_curve(points,
                                                   QColor(r, g, b),
                                                   w)
            else:
                raise MalformedMessageError(message)
        except MalformedMessageError as e:
            self._logger.warning(_translate('MainWindow',
                                            'malformed message: "{}"')
                                 .format(message))

    @pyqtSlot(name='on_pushButton_sendMessage_clicked')
    def send_message(self):
        message = self.ui.lineEdit_message.text()
        if message:
            self.ui.lineEdit_message.clear()
            self.chatclient.send_message('msg:'+ message)
            self.on_chatclient_messageReceived('msg:Me :: ' + message)

    @pyqtSlot()
    def on_lineEdit_message_returnPressed(self):
        if self.chatclient.is_connected:
            self.send_message()

    @pyqtSlot(QPoint, QPoint)
    def on_boardWidget_lineDrawn(self, p1, p2):
        if self.chatclient.is_connected:
            c = self.ui.boardWidget.pen_color
            w = self.ui.boardWidget.pen_width
            message = ('drwl:c({r},{g},{b})#w({width})#p1({p1x},{p1y})#p2({p2x},{p2y})'
                       .format(r=c.red(), g=c.green(), b=c.blue(), width=w,
                               p1x=p1.x(), p1y=p1.y(), p2x=p2.x(), p2y=p2.y()))
            self.chatclient.send_message(message)

    @pyqtSlot(list)
    def on_boardWidget_curveDrawn(self, points):
        if self.chatclient.is_connected:
            c = self.ui.boardWidget.pen_color
            w = self.ui.boardWidget.pen_width
            message = 'drwc:c({r},{g},{b})#w({width})'.format(r=c.red(),
                                                              g=c.green(),
                                                              b=c.blue(),
                                                              width=w)
            for point in points:
                message += '#p0({x},{y})'.format(x=point.x(), y=point.y())
            self.chatclient.send_message(message)

    @pyqtSlot()
    def on_pushButton_clearBoardWidget_clicked(self):
        self.ui.boardWidget.clear_image()

    @pyqtSlot()
    def on_pushButton_colorDialog_clicked(self):
        self.ui.boardWidget.pen_color = QColorDialog.getColor()

    @pyqtSlot(int)
    def on_spinBox_penWidth_valueChanged(self, value):
        self.ui.boardWidget.pen_width = value


def main():
    app = QApplication(sys.argv)

    t = QTranslator()
    t.load("app_ru_RU", 'client')
    app.installTranslator(t)
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    print('This is CCCCCLIENT!')