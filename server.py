import os
import sys
import gettext
import server.board_server

if __name__ == '__main__':
    os.chdir(sys.path[0])
    os.environ['LANG'] = 'ru_RU'
    t = gettext.translation('pyBoards-server', localedir='server/i18n', fallback=True)
    t.install()
    server.board_server.test_board_server()
