import os
from time import strftime, localtime
import logging


def get_logger(module, app='pyBoards'):
    module_logger = logging.getLogger(app)
    module_logger.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_log_formatter = logging.Formatter('%(levelname)-10s : %(name)-30s : %(message)s')
    console_handler.setFormatter(console_log_formatter)
    module_logger.addHandler(console_handler)

    if not os.path.exists('logs'):
        os.mkdir('logs')
    path = os.path.join(os.getcwd(), 'logs')
    fname = '{app}-{module}-{dt}.log'.format(app=app,
                                             module=module,
                                             dt=strftime('%y-%m-%d_%H%M-%S', localtime()))
    fname = os.path.join(path, fname)
    file_handler = logging.FileHandler(fname)
    file_handler.setLevel(logging.INFO)
    file_log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    file_handler.setFormatter(file_log_formatter)
    module_logger.addHandler(file_handler)

    return module_logger
