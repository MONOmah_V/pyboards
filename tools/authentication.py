from random import SystemRandom
from hashlib import sha256
import base64


class AuthenticationError(Exception):
    pass


rand = SystemRandom()


def generate_challenge():
    """
    Generate random 16-byte challenge
    :return: challenge string (not bytes)
    """
    random_str = bytes(map(lambda i: rand.randint(0, 255), range(16)))
    challenge = base64.standard_b64encode(random_str).decode()
    return challenge


def generate_response(sc, cc, login, pword):
    """
    Generate response, based on parameters
    :param sc: server-challenge
    :param cc: client-challenge
    :param login: client-login
    :param pword: client-password
    :return: response string (not bytes)
    """
    return sha256('{}{}{}{}'.format(sc, cc, login, pword).encode()).hexdigest()
