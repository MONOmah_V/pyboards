import enum


class Client():
    class AuthState(enum.Enum):
        none = 0
        sc_sent = 1
        authenticated = 3

    def __init__(self, address, login=None):
        self._auth_state = Client.AuthState.none
        self._address = address
        self._login = login
        self._client_challenge = None
        self._server_challenge = None

    @property
    def address(self):
        return self._address

    @property
    def login(self):
        return self._login if self._login is not None else '{}:{}'.format(*self._address)

    @login.setter
    def login(self, login):
        self._login = login

    @property
    def client_challenge(self):
        return self._client_challenge

    @client_challenge.setter
    def client_challenge(self, client_challenge):
        self._client_challenge = client_challenge

    @property
    def server_challenge(self):
        return self._server_challenge

    @server_challenge.setter
    def server_challenge(self, server_challenge):
        self._server_challenge = server_challenge

    @property
    def auth_state(self):
        return self._auth_state

    @auth_state.setter
    def auth_state(self, state):
        self._auth_state = state