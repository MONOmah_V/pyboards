from struct import pack, unpack
import socket


class msocket(socket.socket):
    """
    Class implements message-based protocol over TCP
    See project documentation for more information
    """
    def __init__(self, sock=None, family=socket.AF_INET,
                 type=socket.SOCK_STREAM, proto=0, fileno=None):
        if sock is not None:
            if sock.getsockopt(socket.SOL_SOCKET,
                               socket.SO_TYPE) != socket.SOCK_STREAM:
                raise NotImplementedError("only stream sockets are supported")
            socket.socket.__init__(self,
                                   family=sock.family,
                                   type=sock.type,
                                   proto=sock.proto,
                                   fileno=sock.fileno())
            self.settimeout(sock.gettimeout())
            sock.detach()
        else:
            socket.socket.__init__(self, family,
                                   type,
                                   proto)

    def accept(self):
        """
        Override base socket.accept() to return our class instance
        :return: msocket-instance of accepted connection and peer address
        """
        newsock, addr = socket.socket.accept(self)
        newsock = msocket(newsock)
        return  newsock, addr

    def send_message(self, data):
        """
        Send message, header and payload in particular
        :param data: encoded (utf-8) message
        """
        length = len(data)
        self.sendall(pack('!I', length))
        self.sendall(data)

    def recv_message(self):
        """
        Receive message
        Receive header with payload length first and then receive payload
        :return: array of bytes of payload
        """
        buffer_length = self.__recv_all(4)
        if not buffer_length:
            # Пришло 0 байт, отключение
            return None
        length, = unpack('!I', buffer_length)
        return self.__recv_all(length)

    def __recv_all(self, count):
        """
        Helper function to receive specified count of bytes from socket
        :param count: number of bytes to receive
        :return: array of bytes
        """
        buf = b''
        while count:
            newbuf = self.recv(count)
            if not newbuf:
                return None
            buf += newbuf
            count -= len(newbuf)
        return buf

