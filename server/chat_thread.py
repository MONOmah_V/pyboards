import threading
import logging
import socket
import selectors
from tools.authentication import AuthenticationError, generate_response, generate_challenge
from server.user_accounts import clients
from tools.account import Client
import tools.msocket as msocket


module_logger = logging.getLogger('pyBoards')


class ChatThread(threading.Thread):
    """
    Class implements chat-room functions. Provides authentication,
    authorization and clients communications
    """
    def __init__(self, listen_address):
        """
        Create chat-room thread and set listen address
        :param listen_address: address on which listen-socket will be bound
        """
        super().__init__()
        self._logger = logging.getLogger(module_logger.name + '.chatter')
        self._stop_event = threading.Event()
        self._listen_address = listen_address
        self._selector = selectors.DefaultSelector()
        self._addr_lock = threading.Lock()
        self._addr_lock.acquire()
        self._listening = False

        self._logger.debug(_('initialized'))

    def __setup_socket(self):
        """
        Create listen-socket and make it listen
        """
        try:
            self._listen_socket = msocket.msocket()
            self._listen_socket.bind(self._listen_address)
            self._listen_socket.setblocking(False)
            self._listen_socket.listen(10)
            self._selector.register(self._listen_socket, selectors.EVENT_READ)
            self._logger.info(_('listening on {}:{}')
                              .format(*self._listen_socket.getsockname()))
            self._listening = True
        except OSError:
            self._logger.exception(_('unable to create listen socket'))
            self._listen_socket.close()
            self._stop_event.set()
        finally:
            self._addr_lock.release()

    def run(self):
        self._logger.info(_('started'))
        self.__setup_socket()
        self.__main_loop()
        self._logger.debug(_('stopped'))

    def stop(self):
        """
        Tell thread to stop
        """
        self._logger.debug(_('STOP received'))
        self._stop_event.set()

    def __main_loop(self):
        """
        Main loop. Multiplexing goes here.
        """
        while not self._stop_event.is_set():
            events = self._selector.select(5)
            for selector_key, mask in events:
                if selector_key.fileobj == self._listen_socket:
                    self.__accept_connection()
                else:
                    try:
                        message = selector_key.fileobj.recv_message()
                        if message:
                            self.__handle_message(selector_key, message)
                        else:
                            self.__close_connection(selector_key)
                    except ConnectionResetError:
                        message = (_('connection reset by peer {}:{}')
                                   .format(*selector_key.data.address))
                        self._logger.warning(message)
                        self.__close_connection(selector_key)
                    except ConnectionAbortedError:
                        message = (_('software at {}:{} caused connection abort')
                                   .format(*selector_key.data.address))
                        self._logger.warning(message)
                        self.__close_connection(selector_key)
                    except ConnectionError as e:
                        self._logger.exception('fixme:' +
                                               _('connection error ') +
                                               e.strerror)
                        self.__close_connection(selector_key)
                    except Exception as e:
                        self._logger.exception('fixme:' +
                                               _('Exception at main loop: ') +
                                               str(e))
                        self.__close_connection(selector_key)
        self.__close_connections()

    def __accept_connection(self):
        """
        Accept connection from client, add it to selector list
        and start authentication
        """
        sock, addr = self._listen_socket.accept()
        sock.setblocking(True)
        client = Client(addr)
        selector_key = self._selector.register(sock,
                                               selectors.EVENT_READ,
                                               data=client)
        self._logger.info(_('accepted {}:{}').format(*addr))
        self.__broadcast_message('srv:' + (_('{} connected')
                                           .format(client.login)),
                                 sock)
        self.__perform_authentication(selector_key, None)

    def __close_connection(self, selector_key, reason=''):
        """
        Close connection to client and tell others, that someone has left the room
        :param selector_key: client to close connection with
        :param reason: optionally reason of disconnection
        """
        addr = selector_key.data.address
        login = selector_key.data.login
        sock = selector_key.fileobj
        try:
            sock.shutdown(socket.SHUT_RDWR)
        except OSError:
            pass
        sock.close()
        self._selector.unregister(sock)
        self._logger.info(_('connection with {}:{} closed').format(*addr))
        message = 'srv:' + _('{} disconnected').format(login)
        if reason:
            message += _(' Reason: ') + reason
        self.__broadcast_message(message, sock)

    def __close_connections(self):
        """
        Shutdown chat-room: close listen-socket and all connections to clients
        """
        with self._addr_lock:
            self._logger.info(_('closing connections'))
            self._listen_socket.close()
            self._logger.debug(_('listen socket closed'))
            try:
                self._selector.unregister(self._listen_socket)
            except ValueError:
                pass
            self.__broadcast_message('term:' + _('Server is shutting down!'))
            for selector_key in self._selector.get_map().values():
                addr = selector_key.data.address
                try:
                    selector_key.fileobj.shutdown(socket.SHUT_RDWR)
                except OSError:
                    pass
                selector_key.fileobj.close()
                self._logger.info(_('connection with {}:{} closed')
                                  .format(*addr))
            self._selector.close()
            self._logger.info(_('connections closed'))

    def __handle_message(self, sender_selector_key, message):
        """
        Implement application protocol, see project documentation for more.
        :param sender_selector_key: message sender
        :param message: ENCODED message
        """
        message = message.decode(errors='replace').rstrip()
        sep = ':'
        header, sep, body = message.partition(sep)
        client = sender_selector_key.data
        sock = sender_selector_key.fileobj
        if not sep or not body:
            self._logger.warning(_('malformed message from {}:{}: "{msg}"')
                                 .format(*client.address, msg=message))
        elif client.auth_state != Client.AuthState.authenticated:
            try:
                self.__perform_authentication(sender_selector_key, message)
            except AuthenticationError as e:
                self._logger.warning(_('authentication failed with {}: {e}')
                                     .format(client.login, e=e))
                self.__send_message(sock, 'term:' +
                                    _('Authentication failed') + ': ' + str(e))
                self.__close_connection(sender_selector_key,
                                        _('Authentication failed'))
        elif header == 'msg':
            self.__broadcast_message('msg:<{}> :: {msg}'
                                     .format(client.login, msg=body), sock)
        elif header in ('drwl', 'drwc'):
            self.__broadcast_message(message, sock)

    def __perform_authentication(self, client_selector_key, message):
        """
        Try to authenticate client. Note: this function is called twice:
        1. Right after accepting connection it sends ServerChallenge
        2. When client sends its response
        Function raises AuthenticationError if something goes wrong
        :param client_selector_key: client to authenticate
        :param message: DECODED message
        """
        client = client_selector_key.data
        sock = client_selector_key.fileobj
        if client.auth_state == Client.AuthState.none:
            challenge = generate_challenge()
            self.__send_message(sock, 'authsc:{}'.format(challenge))
            client.server_challenge = challenge
            client.auth_state = Client.AuthState.sc_sent
        elif client.auth_state == Client.AuthState.sc_sent:
            header, sep, body = message.partition(':')
            if header == 'authcr':  # 'authcr:LOGIN#CHALLENGE#RESPONSE'
                client.login, client.client_challenge, cr = body.split('#')
                if client.login not in clients:
                    raise AuthenticationError(_('user does not exists'))
                sr = generate_response(client.server_challenge,
                                       client.client_challenge,
                                       client.login, clients[client.login])
                if sr == cr:
                    client.auth_state = Client.AuthState.authenticated
                    self._logger.info(_('authenticated {}:{} as {login}')
                                      .format(*client.address,
                                              login=client.login))
                    self.__send_message(sock, 'srv:' +
                                        _('Authentication successful'))
                    self.__broadcast_message('srv:' +
                                             _('{}:{} is now known as {login}')
                                             .format(*client.address,
                                                     login=client.login),
                                             sock)
                else:
                    raise AuthenticationError(
                        _('wrong login/password combination'))
            else:
                raise AuthenticationError(
                    _('bad authentication header and/or message'))

    def __broadcast_message(self, message, sender_sock=None):
        """
        Send message to all clients except sender_sock
        :param message: DECODED message, it will be encoded here
        :param sender_sock: message will be sent to all clients except this one
        """
        for selector_key in self._selector.get_map().values():
            if (selector_key.fileobj != sender_sock and
                    selector_key.fileobj != self._listen_socket):
                selector_key.fileobj.send_message(message.encode())

    def __send_message(self, receiver_sock, message):
        """
        Send message to socket
        :param receiver_sock: message recipient
        :param message: DECODED message
        """
        receiver_sock.send_message(message.encode())

    @property
    def address(self):
        with self._addr_lock:
            return self._listen_socket.getsockname() if self._listening else None


if __name__ == '__main__':
    print('This is ChatThread')
