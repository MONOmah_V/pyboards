#!/bin/bash
source babel_env.sh
retval=0
for lang in $langs; do
    if [[ -f "${localedir}/${lang}/LC_MESSAGES/${project}.po" ]]; then
        $pbl compile --domain="${project}" --locale="${lang}" --directory="${localedir}"
    else
        echo "Template file for \"${lang}\" not found."
        retval=1
    fi
done

exit $retval
