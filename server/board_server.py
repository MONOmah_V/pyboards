import tools.logger
import logging
import argparse
from server.broadcast_server_thread import BroadcastServerThread
from server.chat_thread import ChatThread


module_logger = tools.logger.get_logger('server')


class BoardServer():
    """
    Manager class, responsible for the chat-room and broadcast threads
    """
    # TODO отслеживать успешный старт chat_thread'а
    def __init__(self, listen_address='', listen_port=0):
        """
        Construct manager and start threads
        :param listen_address: specifies IP-address ...
        :param listen_port: ... and port on which chat-room will be served
        """
        self._logger = logging.getLogger(module_logger.name + '.board-server')

        self._listen_address = (listen_address, listen_port)

        self._broadcaster = None
        self._logger.debug(_('initialized'))
        self.__setup_listener()
        self.start_advertising()
        self._logger.info(_('started'))

    def __setup_listener(self):
        """
        Create and start chat-room thread
        """
        self._listener = ChatThread(self._listen_address)
        self._listener.start()

    def __stop_listener(self):
        """
        Stop and wait for termination of chat-room thread
        """
        self._listener.stop()
        if self._listener.is_alive():
            self._listener.join()

    def stop(self):
        """
        Request manager to stop its threads
        """
        self._logger.debug(_('stopping'))
        self.__stop_listener()
        self.stop_advertising(shutting_down=True)
        self._logger.info(_('stopped'))

    def start_advertising(self):
        """
        Start broadcast thread if it is not already running
        """
        if self._listener.address:
            if self._broadcaster is None or not self._broadcaster.is_alive():
                self._broadcaster = BroadcastServerThread(self._listener.address)
                self._broadcaster.start()
            else:
                self._logger.warning(_('broadcaster already running'))

    def stop_advertising(self, shutting_down=False):
        """
        Stop broadcast thread
        """
        if self._broadcaster is None or not self._broadcaster.is_alive():
            if not shutting_down:
                self._logger.warning(_('broadcaster is not running'))
        else:
            self._broadcaster.stop()
            if shutting_down and self._broadcaster.is_alive():
                self._broadcaster.join()


def test_board_server():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--address', default='0.0.0.0', type=str,
                        help=_('IP address of interface to listen on'))
    parser.add_argument('-p', '--port', default=0, type=int,
                        help=_('port of interface to listen on'))
    args = parser.parse_args()
    serv = BoardServer(args.address, args.port)
    commands = {
        1: serv.start_advertising,
        2: serv.stop_advertising,
        3: serv.stop
    }
    while True:
        q = int(input(">"))
        commands[q]()
        if q == 3:
            break

if __name__ == '__main__':
    print('This is BoardServer')
