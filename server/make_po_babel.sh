#!/bin/bash
source babel_env.sh
$pbl extract --charset=$charset --copyright-holder="${name}" --msgid-bugs-address=$email --project=$project --version=$version --output="${potfile}" ./
for lang in $langs; do
    if [[ -f "${localedir}/${lang}/LC_MESSAGES/${project}.po" ]]; then
        $pbl update --domain="${project}" --locale="${lang}" --input-file="${potfile}" --output-dir="${localedir}"
    else
        $pbl init --domain="${project}" --locale="${lang}" --input-file="${potfile}" --output-dir="${localedir}"
    fi
done
