import socket
import threading
from time import sleep
import logging


module_logger = logging.getLogger('pyBoards')


class BroadcastServerThread(threading.Thread):
    """
    Class responsible for broadcasting chat-room's address
    """
    def __init__(self, listener_address, frequency=2, dst_port=13131):
        """
        Create thread for broadcasting and create message for advertising
        :param listener_address: chat-room address tuple (ip, port)
        :param frequency: delay between packets in seconds
        :param dst_port: port for packets to send
        """
        super().__init__()
        self._stop_event = threading.Event()
        self._listener_address = listener_address
        self._frequency = frequency
        self._dst_port = dst_port
        self._message = 'pyBoards-server-v0.1@{}'.format(self._listener_address[1])
        self._logger = logging.getLogger(module_logger.name+'.broadcaster')
        self.__setup_socket()
        self._logger.debug(_('initialized'))

    def __setup_socket(self):
        """
        Create, tweak and bind socket for broadcast
        """
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind((self._listener_address[0], 0))
        self._logger.info(_('bound to {}:{}').format(*self._sock.getsockname()))

    def stop(self):
        """
        Stop thread and close socket
        """
        self._logger.debug(_('STOP received'))
        self._stop_event.set()
        self._sock.close()

    def run(self):
        """
        Main loop - sends broadcast packets
        """
        self._logger.info(_('started'))
        while not self._stop_event.is_set():
            self._sock.sendto(self._message.encode(),
                              ("<broadcast>", self._dst_port))
            sleep(self._frequency)
        self._logger.info(_('stopped'))



if __name__ == '__main__':
    print('This is Broadcaster')
